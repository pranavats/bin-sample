CC=gcc
flags_O0g= -O0 -g
flags_O1g= -O1 -g
flags_O2g= -O2 -g
flags_O3g= -O3 -g
flags_Osg= -Os -g
flags_Ofastg= -Ofast -g

flags_O0= -O0
flags_O1= -O1
flags_O2= -O2
flags_O3= -O3
flags_Os= -Os
flags_Ofast= -Ofast

SRC = $(wildcard src/*/*/*/*/*.c src/*/*/*/*.c src/*/*/*.c src/*/*.c src/*.c)
bin_w_debug_O0= $(patsubst src/%,with-debug/O0/%,$(SRC:.c=))
bin_w_debug_O1= $(patsubst src/%,with-debug/O1/%,$(SRC:.c=))
bin_w_debug_O2= $(patsubst src/%,with-debug/O2/%,$(SRC:.c=))
bin_w_debug_O3= $(patsubst src/%,with-debug/O3/%,$(SRC:.c=))
bin_w_debug_Os= $(patsubst src/%,with-debug/Os/%,$(SRC:.c=))
bin_w_debug_Ofast= $(patsubst src/%,with-debug/Ofast/%,$(SRC:.c=))

bin_wo_debug_O0= $(patsubst src/%,wo-debug/O0/%,$(SRC:.c=))
bin_wo_debug_O1= $(patsubst src/%,wo-debug/O1/%,$(SRC:.c=))
bin_wo_debug_O2= $(patsubst src/%,wo-debug/O2/%,$(SRC:.c=))
bin_wo_debug_O3= $(patsubst src/%,wo-debug/O3/%,$(SRC:.c=))
bin_wo_debug_Os= $(patsubst src/%,wo-debug/Os/%,$(SRC:.c=))
bin_wo_debug_Ofast= $(patsubst src/%,wo-debug/Ofast/%,$(SRC:.c=))


all: $(bin_w_debug_O0) $(bin_w_debug_O1) $(bin_w_debug_O2) $(bin_w_debug_O3) \
$(bin_w_debug_Os) $(bin_w_debug_Ofast) $(bin_wo_debug_O0) $(bin_wo_debug_O1) \
$(bin_wo_debug_O2) $(bin_wo_debug_O3) $(bin_wo_debug_Os) $(bin_wo_debug_Ofast)

with-debug/O0/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O0g) -o $@ $<

with-debug/O1/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O1g) -o $@ $<

with-debug/O2/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O2g) -o $@ $<

with-debug/O3/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O3g) -o $@ $<

with-debug/Os/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_Osg) -o $@ $<

with-debug/Ofast/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_Ofastg) -o $@ $<

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# W/O Debug Symbols

wo-debug/O0/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O0) -o $@ $<

wo-debug/O1/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O1) -o $@ $<

wo-debug/O2/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O2) -o $@ $<

wo-debug/O3/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_O3) -o $@ $<

wo-debug/Os/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_Os) -o $@ $<

wo-debug/Ofast/%:src/%.c
	mkdir -p $(dir $@)
	$(CC) $(flags_Ofast) -o $@ $<

clean:
# rm -rf $(bin_w_debug_O0) $(bin_w_debug_O1) $(bin_w_debug_O2) $(bin_w_debug_O3) $(bin_w_debug_Os) $(bin_wo_debug_O0) $(bin_wo_debug_O1) $(bin_wo_debug_O2) $(bin_wo_debug_O3) $(bin_wo_debug_Os)
	rm -rf  with-debug wo-debug
